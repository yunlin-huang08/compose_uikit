package com.tripreset.compose.uikit.ui.home

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tripreset.compose.ui.screen.ScreenPage
import com.tripreset.compose.uikit.Greeting
import com.tripreset.compose.uikit.ui.theme.ComposeUIKitTheme

@Composable
fun AppIconButton(imageVector: ImageVector, onClick: () -> Unit) {
    IconButton(onClick = onClick) {
        Icon(imageVector, contentDescription = "", tint = Color.White)
    }
}

@Composable
fun AppCommonTopBar(homeViewModel: HomeViewModel) = TopAppBar(
    title = { Text("Simple TopAppBar") },
    navigationIcon = {
        AppIconButton(Icons.Filled.ArrowBack) {
            homeViewModel.onNameChange("1")
        }
    },
    actions = {
        AppIconButton(Icons.Filled.Menu) {
            homeViewModel.onNameChange("2")
        }
        AppIconButton(Icons.Filled.Favorite) {
            homeViewModel.onNameChange("3")
        }
    }
)

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun HomeScreen(helloViewModel: HomeViewModel = viewModel()) {

    ComposeUIKitTheme {
        ScreenPage(topBar = { AppCommonTopBar(helloViewModel) }) {

            val listState = rememberLazyListState()

            LazyColumn(state = listState, modifier = Modifier.fillMaxWidth()) {

                // Add a single item
                item {
                    Greeting(helloViewModel.nameValue())
                }

                // Add 5 items
                items(100) { index ->
                    Greeting(helloViewModel.nameValue())
                }

                // Add another single item
                item {
                    Text(text = "Last item")
                }
            }

            val showButton by remember {
                derivedStateOf {
                    listState.firstVisibleItemIndex > 0
                }
            }

            AnimatedVisibility(visible = showButton) {
            }


        }
    }

}